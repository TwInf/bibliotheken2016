/**
 * Eine Sammelung von werkzeuge, um Kartenspiele zu programmieren.
 * <p>
 * Die Karten Nummern gehen von 2 bis 14 (Ass)
 * Die Karten Farben sind durch die Aufzählung {@link de.twyman.Spielkarten.Spielkarten.Farben}
 * genannt, dort ist eine Joker auch definiert.
 * Bildmengen definitiert die mögliche eingebauet
 * Sammelung von Kartenbilde.  Nicht alle Mengen
 * haben eine Joker, falls keine vorhanden ist
 * kommt kein Bild bei der Abruf.
 * <p>
 * Die Bilder sind alle unter GPL 2
 * abrufbar, der Defaultset stammt von
 * http://www.waste.org/~oxymoron/cards/COPYING
 * Die restliche Bildmenge sind aus der
 * PySol ( <a href="http://www.pysol.org/">http://www.pysol.org/</a>
 * Projekt.
 * <p>
 * Bitte Ruckmeldung an dominic.twyman@gmail.com .
 * @author Dominic Twyman
 */
package de.twyman.Spielkarten;
