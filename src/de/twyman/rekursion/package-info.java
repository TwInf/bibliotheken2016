/**
 * Eine Sammelung von Klassen die bei verschiedene rekursions Aufagben.
 * Hauptsächlich für die 'Springer' und 'Damen' Problemen.
 * <p>
 * Bitte Ruckmeldung an dominic.twyman@gmail.com .
 * @author Dominic Twyman
 */

package de.twyman.rekursion;
