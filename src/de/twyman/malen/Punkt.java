/*
 * Copyright (C) 2014 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.malen;

import java.util.Objects;

/**
 * Ein Punkt, mit X und Y Koordinaten.
 * Mit ein paar hilfs Methoden.
 * @author Twyman
 */
public class Punkt {
    private Integer xKoord;
    private Integer yKoord;
    
    /**
     * Standart Konstruktor.
     * Erstellt eine Punkt an die Uhrsprung.
     */
    public Punkt() {
        xKoord=0;
        yKoord=0;
    } 
    
    /**
     * Konstruktor an eine bestimmte Koordinate.
     * Erstellt eine Punkt an (iX|iY).
     * @param iX der X-Koordinate
     * @param iY der Y-Koordinate
     */
    public Punkt(Integer iX, Integer iY) {
        xKoord=iX;
        yKoord=iY;
    } 
    
    /**
     * Konstruktor an eine bestimmte Punkt.
     * Erstellt eine Punkt an Punkt B.
     * @param B der Punkt zu kopieren.
     */
    public Punkt(Punkt B) {
        xKoord=B.getX();
        yKoord=B.getY();
    } 

    /**
     * Gibt die X-Wert diese Koordinate zurück.
     * @return Integer der X-Koordinate
     */
    public Integer getX() {
        return xKoord;
    }
    
    /**
     * Gibt die Y-Wert diese Koordinate zurück.
     * @return Integer der Y-Koordinate
     */
    public Integer getY() {
        return yKoord;
    }
    
    /**
     * Setzt die X-Wert diese Koordinate.
     * @param iX der neue X-Koordinate
     */
    public void setX(Integer iX) {
        xKoord = iX;
    }

    /**
     * Setzt die Y-Wert diese Koordinate.
     * @param iY der neue Y-Koordinate
     */
    public void setY(Integer iY) {
        yKoord = iY;
    }
    
    /**
     * Gibt die Koordinate als int Reihung zurück.
     * @return Reihung mit zwei elemente, [0]:X-Wert und [1]:Y-Wert
     */
    public int[] getReihung() {
        int[] Koords = new int[2];
        Koords[0] = (xKoord==null)?0:xKoord;
        Koords[1] = (yKoord==null)?0:yKoord;
        return Koords;
    }
    
    /**
     * Berechnet die Mittelpunkt zwischen diese Puntk und Punkt B.
     * @param B der zweite Punkt
     * @return Mittelpunkt von diese Punkt und B.
     */
    public Punkt mittelpunkt(Punkt B) {
        return new Punkt( (xKoord+B.getX())/2
                         ,(yKoord+B.getY())/2);
    }
    
    @Override
    public int hashCode() {
        int hash = 7;
        hash = 97 * hash + Objects.hashCode(this.xKoord);
        hash = 97 * hash + Objects.hashCode(this.yKoord);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Punkt other = (Punkt) obj;
        if (!Objects.equals(this.xKoord, other.xKoord)) {
            return false;
        }
        if (!Objects.equals(this.yKoord, other.yKoord)) {
            return false;
        }
        return true;
    }
}
