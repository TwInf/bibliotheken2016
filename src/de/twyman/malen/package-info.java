/**
 * Eine Sammelung von Klassen die bei verschiedene Malaufgaben helfen.
 * <p>
 * Bitte Ruckmeldung an dominic.twyman@gmail.com .
 * @author Dominic Twyman
 */
package de.twyman.malen;
