/**
 * Die verschiedene Abstraktdatentypen nach
 * der Vorlage von der Kultusministerium
 * in Niedersachsen für das Abitur in Informatik in Jahr 2018.
 * <p>
 * Alle Klassen sind als Generic-Typ implementiert, d.h. es ist möglich z.B.
 * <p><code> Queue&lt;String&gt; instance = new Queue&lt;&gt;();</code>
 * <p>zu programmieren, um eine Queue von Strings zu erzeugen.
 * <p>
 * Alle Klasen sind ohne gewähr!  Ich habe versucht die 
 * Vorgaben aus der Operatorenzettel zu implementieren, 
 * es kann aber sein, dass ich doch eine Fehler 
 * einprogrammiert habe. Vielleicht habe ich auch die Vorgaben 
 * missverstanden. 
 * <p>
 * Bitte Ruckmeldung an dominic.twyman@gmail.com .
 * @author Dominic Twyman
 */
package de.twyman.adts;
