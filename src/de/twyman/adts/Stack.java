/*
 * Copyright (C) 2015 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



package de.twyman.adts;

import java.util.EmptyStackException;
import java.util.Objects;

/**
 * Eine Stack.
 * @author Twyman
 * @param <Inhaltstyp> Der Datentyp der in der Stack gespeichert werden soll.
 */
public class Stack<Inhaltstyp> {
    // Der Java Util Implementierung von eine Stack
    protected java.util.Stack javaStapel;
    
    /**
     * Ein leerer Stapel wird angelegt.
     */
    public Stack() {
        javaStapel = new java.util.Stack<>();
    }

    /**
     * Wenn der Stack kein Element enthält, 
     * wird der Wert <i>wahr</i> zurückgegeben, 
     * sonst der Wert <i>falsch</i>.
     * @return boolean
     */
    public boolean isEmpty() {
        return (javaStapel.isEmpty());
    }

    /**
     * Der Inhalt des obersten Elements des Stacks 
     * wird zurückgegeben, das Element aber nicht entfernt. 
     * @return Inhalt der oberste Element aus der Stack.
     */
    public Inhaltstyp top() {
        Inhaltstyp retVal=null;
        try {
            retVal = (Inhaltstyp) javaStapel.peek();
        }
        catch (EmptyStackException ex) {
            retVal=null;
        }
        return retVal;
    }
    
    /**
     * Ein neues Element mit dem angegebenen 
     * <code>inhalt</code> wird auf den Stack gelegt.
     * @param inhalt der neue Element.
     */
    public void push(Inhaltstyp inhalt) {
        javaStapel.push(inhalt);
    };
    
    /** 
     * Der Inhalt des obersten Elements wird 
     * zurückgegebenund das Element wird entfernt.  
     * @return Inhalt
     */
    public Inhaltstyp pop() {
        return (Inhaltstyp) javaStapel.pop();
    };
    
    /**
     * Hier ist eine Java standard Methode die nicht
     * vom Kultusministerium vorgegeben ist.
     * Die soll nie aufgerufen werden. Ich habe diese
     * Methode implementiert um meine Test-Harness 
     * zu vereinfachen.
     * @return ein Hashcode.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 78 * hash + Objects.hashCode(this.javaStapel);
        return hash;
    }

    /**
     * Hier ist eine Java standard Methode die nicht
     * vom Kultusministerium vorgegeben ist.
     * Die soll nie aufgerufen werden. Ich habe diese
     * Methode implementiert um meine Test-Harness 
     * zu vereinfachen.
     * @return <i>wahr</i> falls die Stack's gleich 
     * sind, sonst <i>falsch</i>.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Stack<?> other = (Stack<?>) obj;
        //
        if (!(this.javaStapel.equals(other.javaStapel))) {
            return false;
        }
        return true;
    }
    
}
