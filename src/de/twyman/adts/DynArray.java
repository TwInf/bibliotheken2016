/*
 * Copyright (C) 2015 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package de.twyman.adts;

import java.util.ArrayList;
import java.util.Objects;

/**
 * Eine dynamische Reihung.
 * <b>Die Nummerierung der dynamischen Reihung 
 * beginnt mit dem Index 1.</b>
 * @author Twyman
 * @param <Inhaltstyp> Der Datentyp der in der dynamische Reihung gespeichert werden soll.
 */
public class DynArray<Inhaltstyp> {
    // Der Java Util Implementierung von eine Liste
    protected ArrayList<Inhaltstyp> javaArrayList;

    /** 
     * Eine leere dynamische Reihung wird angelegt. 
     */
    public DynArray() {
        javaArrayList=new ArrayList<>();
    }
    
    /**
     * Wenn die Reihung kein Element enthält, 
     * wird der Wert <i>wahr</i> zurückgegeben, 
     * sonst der Wert <i>falsch</i>.
     * @return boolean
     */
    public boolean isEmpty() {
        return javaArrayList.isEmpty();
    }
    
    /**
     * Der Inhaltswert des Elements an 
     * der Position <code>index</code> wird zurückgegeben. 
     * @param index Position der Element der geholt werden soll.
     * @return Inhaltstyp aus Position <code>index</code>.
     * @throws IndexOutOfBoundsException falls <code>getLength()&lt;index&lt;1</code>.
    */
    public Inhaltstyp  getItem(int index) {
        return (Inhaltstyp) javaArrayList.get(index-1);
    } 

    /**
     * Die Anzahl der Elemente der dynamischen 
     * Reihung wird zurückgegeben.
     * @return int Anzahl von Elemente in der Reihung
    */
    public int getLength() {
        return javaArrayList.size();
    } 

    /**
     * Ein neues Element mit dem angegebenen 
     * Inhaltswert wird am Ende der dynamischen 
     * Reihung eingefügt. 
     * @param inhalt neue Element
     */
    public void append(Inhaltstyp  inhalt) {
        javaArrayList.add(inhalt);
    }

    /** 
     * Ein neues Element mit dem angegebenen 
     * Inhaltswert wird an der Position <code>index</code>  
     * in die dynamische Reihung eingefügt, 
     * falls an dieser Position schon ein 
     * Element in der Reihung vorhanden ist. 
     * Das sich vorher an dieser Position befindende 
     * Element und alle weiteren werden nach 
     * hinten verschoben. 
     * Ist der Wert von <code>index</code> um eins größer 
     * als die Anzahl der Elemente der Reihung, 
     * wird der Inhaltswert wie bei <code>append</code> am 
     * Ende der dynamischen Reihung eingefügt.
     * Für alle weiteren Werte von <code>index</code>  hat 
     * die Operation keine Wirkung.
     * 
     * @param index Position wo das Element hinein soll.
     * @param inhalt das neue Element
     */
    public void insertAt(int index, Inhaltstyp inhalt) {
        try {
            javaArrayList.add((index-1),inhalt);
        }
        catch (IndexOutOfBoundsException e) {
        } 
    }

    /**
     * Der Inhaltswert des Elements an der Position 
     * <code>index</code> wird durch <code>inhalt</code> ersetzt. 
     * Falls die angegebene Position nicht 
     * existiert, hat die Operation keine Wirkung.
     * @param index Position zu ändern.
     * @param inhalt Neue Inhaltstyp für diese Position.
     */
    public void setItem(int index, Inhaltstyp  inhalt) {
        try {
            javaArrayList.set((index-1),inhalt);
        }
        catch (IndexOutOfBoundsException e) {
        } 
    }
    
    /**
     * Das Element an der Position <code>index</code> wird 
     * entfernt. Alle folgenden Elemente werden 
     * um eine Position nach vorne geschoben. 
     * Falls die angegebene Position nicht 
     * existiert, hat die Operation keine Wirkung.
     * @param index Position zu löschen.
     */
    public void delete(int index) {
        try {
            javaArrayList.remove((index-1));
        }
        catch (IndexOutOfBoundsException e) {
        } 
    }
               
    /**
     * Hier ist eine Java standard Methode die nicht
     * vom Kultusministerium vorgegeben ist.
     * Die soll nie aufgerufen werden. Ich habe diese
     * Methode implementiert um meine Test-Harness 
     * zu vereinfachen.
     * @return ein Hashcode.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 67 * hash + Objects.hashCode(this.javaArrayList);
        return hash;
    }

    /**
     * Hier ist eine Java standard Methode die nicht
     * vom Kultusministerium vorgegeben ist.
     * Die soll nie aufgerufen werden. Ich habe diese
     * Methode implementiert um meine Test-Harness 
     * zu vereinfachen.
     * @return <i>wahr</i> falls die DynArray's gleich 
     * sind, sonst <i>falsch</i>.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final DynArray<?> other = (DynArray<?>) obj;
        //
        if (!(this.javaArrayList.equals(other.javaArrayList))) {
            return false;
        }
        return true;
    }
}
