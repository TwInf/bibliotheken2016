/*
 * Copyright (C) 2015 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package de.twyman.adts;

import java.util.LinkedList;
import java.util.Objects;

/**
 * Eine Queue.
 * @author Twyman
 * @param <Inhaltstyp> Der Datentyp der in der Queue gespeichert werden soll.
 */
public class Queue<Inhaltstyp> {
    // Der Java Util Implementierung von eine Liste
    protected java.util.Queue<Inhaltstyp> javaSchlange;

    /** 
     * Eine leere Schlange wird angelegt. 
     */
    public Queue() {
        javaSchlange=new LinkedList<>();
    }
    
    /**
     * Wenn die Queue kein Element enthält, 
     * wird der Wert <i>wahr</i> zurückgegeben, 
     * sonst der Wert <i>falsch</i>. 
     * @return boolean
     */
    public boolean isEmpty() {
        return javaSchlange.isEmpty();
    }
    
    /**
     * Der <code>inhalt</code> des ersten Elements 
     * der Queue wird zurückgegeben, 
     * das Element aber nicht entfernt. 
     * @return Inhalt
    */
    public Inhaltstyp head() {
        return (Inhaltstyp) javaSchlange.peek();
    } 

    /**
     * Ein neues Element mit dem angegebenen 
     * <code>inhalt</code> wird angelegt und am Ende an die 
     * Queue angehängt. 
     * @param inhalt der neue Element.
     */
    public void enqueue(Inhaltstyp inhalt) {
        javaSchlange.add(inhalt);
    }

    /** 
     * Der Inhalt des ersten Elements wird 
     * zurückgegeben und das Element wird entfernt. 
     * @return Inhalt der erste Element.
     */
    public Inhaltstyp dequeue() {
        return (Inhaltstyp) javaSchlange.remove();
    }
    
           
    /**
     * Hier ist eine Java standard Methode die nicht
     * vom Kultusministerium vorgegeben ist.
     * Die soll nie aufgerufen werden. Ich habe diese
     * Methode implementiert um meine Test-Harness 
     * zu vereinfachen.
     * @return ein Hashcode.
     */
    @Override
    public int hashCode() {
        int hash = 3;
        hash = 34 * hash + Objects.hashCode(this.javaSchlange);
        return hash;
    }

    /**
     * Hier ist eine Java standard Methode die nicht
     * vom Kultusministerium vorgegeben ist.
     * Die soll nie aufgerufen werden. Ich habe diese
     * Methode implementiert um meine Test-Harness 
     * zu vereinfachen.
     * @return <i>wahr</i> falls die Queue's gleich 
     * sind, sonst <i>falsch</i>.
     */
    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Queue<?> other = (Queue<?>) obj;
        //
        if (!(this.javaSchlange.equals(other.javaSchlange))) {
            return false;
        }
        return true;
    }
}
