/*
 * Copyright (C) 2015 D.Twyman
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


package de.twyman.adts;

/**
 * Eine Binärbaum.
 * @author Twyman
 * @param <Inhaltstyp> Der Datentyp der in der Baum gespeichert werden soll.
 */
public class BinTree<Inhaltstyp> {
    // der Inhaltstyp von eine BaumNode
    class BaumElement
    {
        Inhaltstyp NodeInhalt = null;
        BinTree linksUnterbaum = null;
        BinTree rechtsUnterbaum = null;
    }
    private BaumElement Würzel = new BaumElement();
    
    /** 
     * Ein leere Baum wird erzeugt.
     */
    public BinTree() {
    }

    /** Ein Baum wird erzeugt. Die Wurzel 
     * erhält den übergebenen <code>inhalt</code> als Wert. 
     * @param inhalt der neue inhalt der Wurzel.
     */
    public BinTree(Inhaltstyp inhalt) {
        Würzel.NodeInhalt = inhalt;
    }
    
    /**
     * Die Anfrage liefert den Wert wahrBinTree
     * wenn der BinTree keine Nachfolger hat, 
     * sonst liefertsie den Wert falsch. 
     * @return boolean
     */
    public boolean isLeaf() {
        return ((Würzel.linksUnterbaum  == null) &&
                (Würzel.rechtsUnterbaum == null)   );
    }

    /**
     * Die Operation gibt den linken Teilbaum zurück. 
     * Existiert kein linker Nachfolger, 
     * so ist das Ergebnis null. 
     * @return BinTree
     */
    public BinTree getLeft() {
        return Würzel.linksUnterbaum;
    }

    /**
     * Die Operation gibt den rechten Teilbaum zurück. 
     * Existiert kein rechter Nachfolger, 
     * so ist das Ergebnis null. 
     * @return BinTree
     */
    public BinTree getRight() {
        return Würzel.rechtsUnterbaum;
    }
    
    /**
     * Der übergebene BinTree wird als linker 
     * Teilbaum gesetzt. 
     * @param b der neue linke Baumteil.
     */
    public void setLeft(BinTree b) {
        Würzel.linksUnterbaum = b;
    }

    /**
     * Der übergebene BinTree wird als rechter 
     * Teilbaum gesetzt.
     * @param b der neue rechte Baumteil.
     */
    public void setRight(BinTree b) {
        Würzel.rechtsUnterbaum = b;
    }
    
    /**
     * Die Operation gibt den Inhaltswert 
     * der Wurzel des aktuellen BinTree zurück. 
     * @return Inhalt der inhalt des Wurzels.
     */
    public Inhaltstyp getItem() {
        return (Inhaltstyp) Würzel.NodeInhalt;
    }

    /**
     * Die Operation setzt den Inhaltswert der 
     * Wurzel des aktuellen BinTreees.     
     * @param inhalt der neue inhalt des Wurzels.
     */
    public void setItem(Inhaltstyp inhalt)  {
        Würzel.NodeInhalt = inhalt;
    }
}
