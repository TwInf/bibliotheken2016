/*
 * Copyright (C) 2015 dominic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts.test;

import de.twyman.adts.Stack;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dominic
 */
public class StackTest {
    
    Random rnd;
    
    public StackTest() {
        rnd = new Random();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isEmpty method, of class Stack.
     */
    @Test
    public void testisEmpty() {
        Stack instance = new Stack();
        assertTrue(instance.isEmpty());
        instance.push(23);
        assertFalse(instance.isEmpty());
    }

    /**
     * Test of top method, of class Stack.
     */
    @Test
    public void testtoppoppush() {
        Stack<Integer> instance = new Stack<>();

        instance.push(23);
        instance.push(32);
        assertEquals((Integer)32,instance.top());
        Integer RetWert;
        RetWert = instance.pop();
        assertEquals((Integer)32,RetWert);
        assertEquals((Integer)23,instance.top());
        RetWert = instance.top();
        assertEquals((Integer)23,RetWert);

        instance.push(43);
        assertEquals((Integer)43,instance.top());
        RetWert = instance.pop();
        assertEquals((Integer)43,RetWert);
        assertEquals((Integer)23,instance.pop());
        assertEquals(null,instance.top());
    }

    /**
     * Test of top method, of class Stack.
     */
    @Test
    public void testpush() {
        Stack<Integer> instance = new Stack<>();

        instance.push(23);
        instance.push(32);
        assertEquals((Integer)32,instance.top());
        instance.push(43);
        assertEquals((Integer)43,instance.top());
        assertEquals((Integer)43,instance.top());
    }

    /**
     * Test of hashCode method, of class Stack.
     */
    @Test
    public void testHashCodeEquals() {
        // die sind nicht gleich
        Stack<Integer> a = new Stack<Integer>();
        Stack<Integer> b = new Stack<Integer>();
        
        // die drei sind alle gleich
        Stack<Integer> x = new Stack<Integer>();
        Stack<Integer> y = new Stack<Integer>();
        Stack<Integer> z = new Stack<Integer>();

        // alle haben zwischen 10 und 20 Zahlen
        int j= rnd.nextInt(10)+10;
        for(int i=0; i<j; i++) {
            // die Zahlen sind zwischen -500 und 500
            Integer k = rnd.nextInt(1000)-500;
            x.push(k);
            y.push(k);
            z.push(k);

            // ab und zu ist bei a ein zahl anderes.
            // aber das dritte ist immer anderes
            a.push(k+rnd.nextInt(2)+((i==2)?1:0));
            // b ist gleich
            b.push(k);
        }
        // b hat aber noch ein element
        b.push(23);

        // gleichheits regelen beachten
        assertTrue(   x.equals(y) && y.equals(x));
        assertTrue(   y.equals(z) && z.equals(y));
        assertTrue(   x.equals(z) && z.equals(x));
        assertTrue(   x.equals(x));
        
        assertFalse(  a.equals(x)  );
        assertFalse(  x.equals(a)  );
        assertFalse(  b.equals(x)  );
        assertFalse(  x.equals(b)  );
        assertFalse(  x.equals(rnd));
        
        // hashcode regelen beachten
        assertTrue(  x.hashCode() == y.hashCode());        
        assertTrue(  y.hashCode() == z.hashCode());        
        assertTrue(  z.hashCode() == x.hashCode());        
        
        //könnte sein, sollte aber wirklich nicht
        assertFalse( a.hashCode() == x.hashCode());        
        assertFalse( b.hashCode() == x.hashCode());        
    }
    
}
