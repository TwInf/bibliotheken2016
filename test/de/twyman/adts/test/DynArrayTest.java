/*
 * Copyright (C) 2015 dominic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts.test;

import de.twyman.adts.DynArray;
import java.util.NoSuchElementException;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dominic
 */
public class DynArrayTest {
    
    Random rnd;
    
    public DynArrayTest() {
        rnd = new Random();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isEmpty method, of class DynArray.
     */
    @Test
    public void testisEmpty() {
        DynArray<String> instance = new DynArray<>();
        assertTrue(instance.isEmpty());
        instance.append("a");
        assertFalse(instance.isEmpty());
    }

    /**
     * Test of getLength method, of class DynArray.
     */
    @Test
    public void testgetLengthdelete() {
        DynArray<String> instance = new DynArray<>();
        assertEquals(0,instance.getLength());
        instance.append("a");
        assertEquals(1,instance.getLength());
        instance.append("b");
        instance.append("c");
        assertEquals(3,instance.getLength());
        instance.delete(1);
        assertEquals(2,instance.getLength());
        instance.delete(100);
        assertEquals(2,instance.getLength());
        instance.delete(1);
        assertEquals(1,instance.getLength());
        instance.delete(1);
        assertEquals(0,instance.getLength());
    }
    /**
     * Test of get method, of class DynArray.
     */
    @Test
    public void testappendgetdelete() {
        DynArray<Integer> instance = new DynArray<>();
        instance.append(23);
        instance.append(32);
        assertEquals((Integer)23,instance.getItem(1));
        instance.append(43);
        assertEquals((Integer)23,instance.getItem(1));
        instance.delete(1);
        assertEquals((Integer)32,instance.getItem(1));
    }

    /**
     * Test of equals method, of class DynArray.
     */
    @Test
    public void testHashCodeEquals() {
        // die sind nicht gleich
        DynArray<Integer> a = new DynArray<Integer>();
        DynArray<Integer> b = new DynArray<Integer>();
        
        // die drei sind alle gleich
        DynArray<Integer> x = new DynArray<Integer>();
        DynArray<Integer> y = new DynArray<Integer>();
        DynArray<Integer> z = new DynArray<Integer>();

        // alle haben zwischen 10 und 20 Zahlen
        int j= rnd.nextInt(10)+10;
        for(int i=0; i<j; i++) {
            // die Zahlen sind zwischen -500 und 500
            Integer k = rnd.nextInt(1000)-500;
            x.append(k);
            y.append(k);
            z.append(k);

            // ab und zu ist bei a ein zahl anderes.
            // aber das dritte ist immer anderes
            a.append(k+rnd.nextInt(2)+((i==2)?1:0));
            // b ist gleich
            b.append(k);
        }
        // b hat aber noch ein element
        b.append(23);

        // gleichheits regelen beachten
        assertTrue(   x.equals(y) && y.equals(x));
        assertTrue(   y.equals(z) && z.equals(y));
        assertTrue(   x.equals(z) && z.equals(x));
        assertTrue(   x.equals(x));
        
        assertFalse(  a.equals(x)  );
        assertFalse(  x.equals(a)  );
        assertFalse(  b.equals(x)  );
        assertFalse(  x.equals(b)  );
        assertFalse(  x.equals(rnd));
        
        // hashcode regelen beachten
        assertTrue(  x.hashCode() == y.hashCode());        
        assertTrue(  y.hashCode() == z.hashCode());        
        assertTrue(  z.hashCode() == x.hashCode());        
        
        //könnte sein, sollte aber wirklich nicht
        assertFalse( a.hashCode() == x.hashCode());        
        assertFalse( b.hashCode() == x.hashCode());        
    }
    
}
