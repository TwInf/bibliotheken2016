/*
 * Copyright (C) 2015 dominic
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package de.twyman.adts.test;

import de.twyman.adts.Queue;
import java.util.NoSuchElementException;
import java.util.Random;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author dominic
 */
public class QueueTest {
    
    Random rnd;
    
    public QueueTest() {
        rnd = new Random();
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of isEmpty method, of class Queue.
     */
    @Test
    public void testisEmpty() {
        Queue<String> instance = new Queue<>();
        assertTrue(instance.isEmpty());
        instance.enqueue("a");
        assertFalse(instance.isEmpty());
    }

    /**
     * Test of head method, of class Queue.
     */
    @Test
    public void testenqueuehead() {
        Queue<Integer> instance = new Queue<>();
        instance.enqueue(23);
        instance.enqueue(32);
        assertEquals((Integer)23,instance.head());
        instance.enqueue(43);
        assertEquals((Integer)23,instance.head());
        instance.dequeue();
        assertEquals((Integer)32,instance.head());
    }


    
    /**
     * Test of dequeue method, of class Queue.
     */
    @Test
    public void testdequeue() {
        Queue<Integer> instance = new Queue<>();

        instance.enqueue(23);
        instance.enqueue(32);
        assertEquals((Integer)23,instance.head());
        Integer RetWert;
        RetWert = instance.dequeue();
        assertEquals((Integer)23,RetWert);
        assertEquals((Integer)32,instance.head());
        RetWert = instance.dequeue();
        assertEquals((Integer)32,RetWert);

        instance.enqueue(43);
        assertEquals((Integer)43,instance.head());
        RetWert = instance.dequeue();
        assertEquals((Integer)43,RetWert);
        assertEquals(null,instance.head());
    }

    /**
     * Test of dequeue method, of class Queue.
     */
    @Test(expected = NoSuchElementException.class)
    public void testdequeueLeer() {
        Queue<Integer> instance = new Queue<>();

        instance.dequeue();
    }    
    

    /**
     * Test of equals method, of class Queue.
     */
    @Test
    public void testHashCodeEquals() {
        // die sind nicht gleich
        Queue<Integer> a = new Queue<Integer>();
        Queue<Integer> b = new Queue<Integer>();
        
        // die drei sind alle gleich
        Queue<Integer> x = new Queue<Integer>();
        Queue<Integer> y = new Queue<Integer>();
        Queue<Integer> z = new Queue<Integer>();

        // alle haben zwischen 10 und 20 Zahlen
        int j= rnd.nextInt(10)+10;
        for(int i=0; i<j; i++) {
            // die Zahlen sind zwischen -500 und 500
            Integer k = rnd.nextInt(1000)-500;
            x.enqueue(k);
            y.enqueue(k);
            z.enqueue(k);

            // ab und zu ist bei a ein zahl anderes.
            // aber das dritte ist immer anderes
            a.enqueue(k+rnd.nextInt(2)+((i==2)?1:0));
            // b ist gleich
            b.enqueue(k);
        }
        // b hat aber noch ein element
        b.enqueue(23);

        // gleichheits regelen beachten
        assertTrue(   x.equals(y) && y.equals(x));
        assertTrue(   y.equals(z) && z.equals(y));
        assertTrue(   x.equals(z) && z.equals(x));
        assertTrue(   x.equals(x));
        
        assertFalse(  a.equals(x)  );
        assertFalse(  x.equals(a)  );
        assertFalse(  b.equals(x)  );
        assertFalse(  x.equals(b)  );
        assertFalse(  x.equals(rnd));
        
        // hashcode regelen beachten
        assertTrue(  x.hashCode() == y.hashCode());        
        assertTrue(  y.hashCode() == z.hashCode());        
        assertTrue(  z.hashCode() == x.hashCode());        
        
        //könnte sein, sollte aber wirklich nicht
        assertFalse( a.hashCode() == x.hashCode());        
        assertFalse( b.hashCode() == x.hashCode());        
    }
    
}
